#!/usr/bin/python

import datetime
import time
import requests
import re
import random


def login():
    base_url = 'http://lapolladefutbol2018.com'

    # Login
    url = base_url + '/interfaz/in-check.php'
    payload = {
        'webuser': '99999999',
        'webpass': '99999999',
    }
    print 'login ' + url
    with requests.Session() as s:
        p = s.post(url, data=payload)
        print p.text
        r = s.get(base_url + '/interfaz/consulta_partidos_1_nuevo.php')
        print r.text.encode('utf-8')
        print r.cookies
    # we need the session cookie
    return 'success'


def http_test():
    cookies = login()

    base_url = 'http://lapolladefutbol2018.com'

    # generate sales report
    url = base_url + '/interfaz/consulta_partidos_1_nuevo.php'

    # need to pass the cookies in order to tell the server that I'm "that" person
    # this is GET request, because need to get the CSRF token before can do a POST
    res = requests.get(url, cookies=cookies)
    html = res.text.encode('utf-8')
    print html
    # the "pattern" is depends on how you construct your html page
    #pattern = 'name="csrf-token" content="(\w+)"'
    #matches = re.findall(pattern, html)
    #token = matches[0]

    # POST to generate sales report
    #payload = {
    #    'date': '2016-06-27',
    #    '_token': token,
    #    'sales_id': random.randint(1, 30),
    #}
    #res = requests.post(url, cookies=cookies, data=payload)

def main():
    for i in range(0, 10):
        print (datetime.datetime.now(),)
        login()
        #time.sleep(1) # every 1 second submit http requests {login, GET report, POST report}

if __name__ == "__main__":
    main()



